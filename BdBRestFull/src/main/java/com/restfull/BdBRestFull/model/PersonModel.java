package com.restfull.BdBRestFull.model;

import java.util.Date;

import com.restfull.BdBRestFull.entity.Person;

public class PersonModel 
{
	public PersonModel(Person person) {
		this.id = person.getId();
		this.name = person.getName();
		this.lastName = person.getLastName();
		this.birth = person.getBirth();
	}
	
	public PersonModel(int Id, String Name, String LastName, Date Birth) {
		super();
		id = Id;
		name = Name;
		lastName = LastName;
		birth = Birth;
	}
	
	private int id;
	private String name;
	private String lastName;
	private Date birth;
	
	public int getId() {
		return id;
	}
	public void setId(int Id) {
		id = Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String Name) {
		name = Name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String LastName) {
		lastName = LastName;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date Birth) {
		birth = Birth;
	}
}