package com.restfull.BdBRestFull.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.restfull.BdBRestFull.model.PersonModel;
import com.restfull.BdBRestFull.entity.Person;

@Component("converter")
public class Converter {
	
	public List<PersonModel> convertList(List<Person> persons){
		
		List<PersonModel> mpersons = new ArrayList<>();
		
		for(Person person : persons) {
			mpersons.add(new PersonModel(person));
		}
		return mpersons;
	}
}
