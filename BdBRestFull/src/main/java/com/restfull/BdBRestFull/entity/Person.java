package com.restfull.BdBRestFull.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "Person")
@Entity
public class Person implements Serializable {
	public Person() {

	}

	public Person(int Id, String Name, String LastName, Date Birth) {
		super();
		id = Id;
		name = Name;
		lastName = LastName;
		birth = Birth;
	}

	@GeneratedValue
	@Id
	@Column(name = "IdPerson")
	private int id;

	@Column(name = "namePerson")
	private String name;

	@Column(name = "LastnamePerson")
	private String lastName;

	@Column(name = "Birth")
	private Date birth;

	public int getId() {
		return id;
	}

	public void setId(int Id) {
		id = Id;
	}

	public String getName() {
		return name;
	}

	public void setName(String Name) {
		name = Name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastname(String LastName) {
		lastName = LastName;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date Birth) {
		birth = Birth;
	}
}