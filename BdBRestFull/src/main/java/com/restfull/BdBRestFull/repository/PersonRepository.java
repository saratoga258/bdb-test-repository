package com.restfull.BdBRestFull.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restfull.BdBRestFull.entity.Person;

@Repository("repository")
public interface PersonRepository extends JpaRepository<Person, Serializable>{
	
	public abstract List<Person> findByName(String name);
	
	public abstract Person findByNameAndId(String name, int id);
}