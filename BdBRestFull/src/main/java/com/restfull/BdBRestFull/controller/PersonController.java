package com.restfull.BdBRestFull.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restfull.BdBRestFull.entity.Person;
import com.restfull.BdBRestFull.model.PersonModel;
import com.restfull.BdBRestFull.service.PersonService;

@RestController
@RequestMapping("/v1")
public class PersonController {
	
	@Autowired
	@Qualifier("service")
	PersonService service;
	
	@PutMapping("/person")
	public boolean addPerson(@RequestBody @Validated Person person) {
		return service.create(person);
	}
	
	@PostMapping("/person")
	public boolean updatePerson(@RequestBody @Validated Person person) {
		return service.update(person);
	}
	
	@DeleteMapping("/person/{id}/{name}")
	public boolean deletePerson(@PathVariable("id") int id, @PathVariable("name") String name) {
		return service.delete(name, id);
	}
	
	@GetMapping("/persons")
	public List<PersonModel> getAllPersons(){
		return service.getAll();
	}
}
