package com.restfull.BdBRestFull.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.restfull.BdBRestFull.converter.Converter;
import com.restfull.BdBRestFull.entity.Person;
import com.restfull.BdBRestFull.model.PersonModel;
import com.restfull.BdBRestFull.repository.PersonRepository;

@Service("service")
public class PersonService {
	
	 @Autowired
	 @Qualifier("repository")
	 private PersonRepository repository;
	 
	 @Autowired
	 @Qualifier("converter")
	 private Converter converter;
	 
	 public boolean create(Person person){
		 try {
			 repository.save(person);
			 return true;
		 }
		 catch (Exception e) {
			return false;
		}
	 }
	 
	 public boolean update(Person person){
		 try {
			 repository.save(person);
			 return true;
		 }
		 catch (Exception e) {
			return false;
		}
	 }
	 
	 public boolean delete(String name, int id){
		 try {
			 Person person = repository.findByNameAndId(name, id);
			 repository.delete(person);
			 return true;
		 }
		 catch (Exception e) {
			return false;
		}
	 }
	 
	 public List<PersonModel> getAll(){
		 return converter.convertList(repository.findAll());
	 }
	 
	 public List<PersonModel> getByName(String name){
		 return converter.convertList(repository.findByName(name));
	 }
	 
	 public PersonModel getByNameAndId(String name, int id) {
		 return new PersonModel(repository.findByNameAndId(name, id));
	 }
}
