import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPersonComponent } from './modules/person/list-person/list-person.component';
import { CreatePersonComponent } from './modules/person/create-person/create-person.component';

const routes: Routes = [
  { path: '', component: ListPersonComponent},
  { path: 'createPerson', component: CreatePersonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [CreatePersonComponent, ListPersonComponent];