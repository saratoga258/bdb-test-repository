import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { person } from 'src/app/models/person';
import { ServiceService } from '../../../services/service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.css']
})
export class CreatePersonComponent implements OnInit {

  form: FormGroup;

  constructor(
    public service: ServiceService, 
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: null,
      lastName: null,
      birth: null
    });
  }

  back(): void{
    this.router.navigate(['']);
  }

  save(){
    console.log(this.form.value)
    this.service.createPerson(this.form.value).subscribe(response => {
      this.back();
    });
  }
}
