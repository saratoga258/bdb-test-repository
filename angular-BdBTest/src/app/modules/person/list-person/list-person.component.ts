import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../services/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-person',
  templateUrl: './list-person.component.html',
  styleUrls: ['./list-person.component.css']
})
export class ListPersonComponent implements OnInit {

  public persons = {};
  displayedColumns: string[] = ['name', 'lastName', 'birth'];
  constructor(public service: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this.persons = [];
    this.getAllPerson();
  }

  getAllPerson() {
    this.service.getAllPersons().subscribe(response => {
      console.log(response);
      this.persons = response;
    });
  }

  newPerson(): void{
    this.router.navigate(['/createPerson']);
  }
}
