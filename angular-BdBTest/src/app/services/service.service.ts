import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { person } from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(protected http: HttpClient) { }

  getAllPersons() {
    return this.http.get('http://localhost:8090/v1/persons/');
  }

  createPerson(data: person){
    return this.http.post('http://localhost:8090/v1/person', data);
  }
}
